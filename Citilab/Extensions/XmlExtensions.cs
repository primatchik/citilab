﻿using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Citilab.Extensions
{
    internal static class XmlExtensions
    {
        public static string SerializeToXml<T>(this T value)
        {
            if (value == null)
                return string.Empty;

            var xmlSerializer = new XmlSerializer(typeof(T));

            using var memoryStream = new MemoryStream();
            using var xmlWriter = XmlWriter.Create(memoryStream, new XmlWriterSettings { Indent = true, Encoding = Encoding.UTF8, OmitXmlDeclaration = true });
            
            xmlSerializer.Serialize(xmlWriter, value);
            
            return Encoding.UTF8.GetString(memoryStream.ToArray());
        }

        public static T DeserializeFromXml<T>(this string value)
        {
            using TextReader textReader = new StringReader(value);
            using XmlTextReader reader = new XmlTextReader(textReader);

            reader.Namespaces = false;

            XmlSerializer serializer = new XmlSerializer(typeof(T));

            return (T)serializer.Deserialize(reader);
        }
    }
}
