﻿using Citilab.Extensions;
using Citilab.Payloads;
using Microsoft.Extensions.Options;
using System.Text;

namespace Citilab.Infra
{
    public interface ICitilabConnector
    {
        Task<GetInquiriesJournal_response_unwrapped> GetInquiriesJournalAsync(DateTime from, DateTime to);
    }

    internal class CitilabConnector : ICitilabConnector
    {
        readonly ILogger<CitilabConnector> _logger;
        readonly IHttpClientFactory _clientFactory;
        readonly ConnectorSettings _connectorSettings;

        public CitilabConnector(
            ILogger<CitilabConnector> logger,
            IHttpClientFactory clientFactory,
            IOptions<ConnectorSettings> connectorSettings)
        {
            _logger = logger;
            _clientFactory = clientFactory;
            _connectorSettings = connectorSettings.Value;
        }

        public async Task<GetInquiriesJournal_response_unwrapped> GetInquiriesJournalAsync(DateTime from, DateTime to)
        {
            var requestValue = new GetInquiriesJournal_request();
            requestValue.E.Add(new E { N = "login", T = "string", V = _connectorSettings.Login });
            requestValue.E.Add(new E { N = "password", T = "string", V = _connectorSettings.Password });
            requestValue.E.Add(new E { N = "fromDate", T = "datetime", V = from.ToString("dd/MM/yyyy HH:mm") });
            requestValue.E.Add(new E { N = "tillDate", T = "datetime", V = to.ToString("dd/MM/yyyy HH:mm") });

            using var httpClient = _clientFactory.CreateClient("GetInquiriesJournal");

            using var request = new HttpRequestMessage(HttpMethod.Post, "");
            request.Content = new StringContent(requestValue.SerializeToXml(), Encoding.UTF8, "text/xml");

            HttpResponseMessage response = null;

            _logger.LogTrace($"{DateTime.UtcNow} - Request started");

            try
            {
                response = await httpClient.SendAsync(request).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"{DateTime.UtcNow} - Unable to receive response from Citilab.");
                throw;
            }

            if (response == null)
            {
                _logger.LogError($"{DateTime.UtcNow} - Null response received from Citilab");
                throw new Exception("Null response received from Citilab");
            }

            GetInquiriesJournal_response result = null;
            var stringResponse = String.Empty;

            try
            {
                stringResponse = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                result = stringResponse.DeserializeFromXml<GetInquiriesJournal_response>();
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"{DateTime.UtcNow} - Unable to deserialize xml response. Response: {stringResponse}");
                throw;
            }

            _logger.LogTrace($"{DateTime.UtcNow} - Request finished, body: {stringResponse}");

            return Unwrap(result);
        }

        static GetInquiriesJournal_response_unwrapped Unwrap(GetInquiriesJournal_response @params)
        {
            string s = Encoding.GetEncoding("windows-1251").GetString(Convert.FromBase64String(@params.Text));

            return s.DeserializeFromXml<GetInquiriesJournal_response_unwrapped>();
        }
    }
}
