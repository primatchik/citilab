﻿namespace Citilab.Infra
{
    public class ConnectorSettings
    {
        public const string ConfigurationSectionName = "CitilabSettings:HttpClient";
        public string Url { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
