﻿using Citilab.Payloads;

namespace Citilab.Infra
{
    public interface IInquiriesJournalFilter
    {
        List<Order> FilterByServices(List<Order> unfilteredOrders, string servies);
    }

    internal class InquiriesJournalFilter : IInquiriesJournalFilter
    {
        public List<Order> FilterByServices(List<Order> unfilteredOrders, string servies)
        {
            var servList = servies.Split(";");

            return unfilteredOrders.Where(x =>
                x.S.Any(y => y.N.ToLower() == "targets" &&
                y.R.Any(z => servList.Contains(z.I)))).ToList();
        }
    }
}
