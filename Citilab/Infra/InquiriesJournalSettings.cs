﻿namespace Citilab.Infra
{
    public class InquiriesJournalSettings
    {
        public const string ConfigurationSectionName = "CitilabSettings:GetInquiriesJournal";
        public string MethodName { get; set; }
        public int TimeoutInMinutes { get; set; }
        public int RequestStepInMinutes { get; set; }
        public DateTime StartFrom { get; set; }
        public DateTime EndTo { get; set; }
        public string Filter { get; set; }
        public string PathToSave { get; set; }
    }
}
