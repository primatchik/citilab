﻿namespace Citilab.Infra.Savers
{
    internal class FileSaver : ISaver
    {
        public async Task SaveAsync(string fileName, object data)
        {
            if(!Directory.Exists(Path.GetDirectoryName(fileName)))
                Directory.CreateDirectory(Path.GetDirectoryName(fileName));

            await File.WriteAllTextAsync(fileName, data.ToString());
        }
    }
}
