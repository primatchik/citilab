﻿namespace Citilab.Infra.Savers
{
    public interface ISaver
    {
        Task SaveAsync(string fileName, object data);
    }
}
