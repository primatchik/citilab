﻿using System.Xml.Serialization;

namespace Citilab.Payloads
{
    [XmlRoot(ElementName = "e")]
	public class E
	{

		[XmlAttribute(AttributeName = "n")]
		public string N { get; set; }

		[XmlAttribute(AttributeName = "v")]
		public string V { get; set; }

		[XmlAttribute(AttributeName = "t")]
		public string T { get; set; }
	}

	[XmlRoot(ElementName = "content")]
	public class GetInquiriesJournal_request
	{

		[XmlElement(ElementName = "e")]
		public List<E> E { get; set; } = new List<E>();
	}
}
