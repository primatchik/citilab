﻿using System.Xml.Serialization;

namespace Citilab.Payloads
{
    [XmlRoot(ElementName = "string")]
	public class GetInquiriesJournal_response
	{

		[XmlAttribute(AttributeName = "xmlns")]
		public string Xmlns { get; set; }

		[XmlText]
		public string Text { get; set; }
	}
}
