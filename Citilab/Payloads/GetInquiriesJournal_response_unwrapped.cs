﻿using System.Xml.Serialization;

namespace Citilab.Payloads
{
    [XmlRoot(ElementName = "f")]
	public class F
	{

		[XmlAttribute(AttributeName = "n")]
		public string N { get; set; }

		[XmlAttribute(AttributeName = "v")]
		public string V { get; set; }
	}

	[XmlRoot(ElementName = "r")]
	public class R
	{

		[XmlAttribute(AttributeName = "n")]
		public string N { get; set; }

		[XmlAttribute(AttributeName = "i")]
		public string I { get; set; }
	}

	[XmlRoot(ElementName = "s")]
	public class S
	{

		[XmlElement(ElementName = "r")]
		public List<R> R { get; set; }

		[XmlAttribute(AttributeName = "n")]
		public string N { get; set; }
	}

	[XmlRoot(ElementName = "o")]
	public class Order
	{

		[XmlElement(ElementName = "f")]
		public List<F> F { get; set; }

		[XmlElement(ElementName = "r")]
		public R R { get; set; }

		[XmlElement(ElementName = "s")]
		public List<S> S { get; set; }
	}

	[XmlRoot(ElementName = "content")]
	public class GetInquiriesJournal_response_unwrapped
	{

		[XmlElement(ElementName = "o")]
		public List<Order> O { get; set; }
	}
}
