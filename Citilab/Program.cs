using Citilab;
using Citilab.Infra;
using Microsoft.Extensions.Options;
using Serilog;

IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices((ctx, services) =>
    {
        services.AddHostedService<Worker>();

        services.Configure<ConnectorSettings>(ctx.Configuration.GetSection(ConnectorSettings.ConfigurationSectionName));
        services.Configure<InquiriesJournalSettings>(ctx.Configuration.GetSection(InquiriesJournalSettings.ConfigurationSectionName));

        services.AddHttpClient("GetInquiriesJournal", (sp, x) =>
        {
            var connectorSettings = sp.GetRequiredService<IOptions<ConnectorSettings>>().Value;
            var getInquiriesJournalSettings = sp.GetRequiredService<IOptions<InquiriesJournalSettings>>().Value;

            x.BaseAddress = new Uri(connectorSettings.Url + getInquiriesJournalSettings.MethodName);
            x.Timeout = TimeSpan.FromSeconds(300);
        });

        services.AddSingleton<ICitilabConnector, CitilabConnector>();
    })
    .UseSerilog((ctx, lc) => lc
        .WriteTo.Console()
        .WriteTo.File("log.txt", rollingInterval: RollingInterval.Day)
        .ReadFrom.Configuration(ctx.Configuration))
    .Build();

await host.RunAsync();
