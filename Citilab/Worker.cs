using Citilab.Extensions;
using Citilab.Infra;
using Citilab.Infra.Savers;
using Citilab.Payloads;
using Microsoft.Extensions.Options;
using System.Text;

namespace Citilab;

public class Worker : BackgroundService
{
    readonly ILogger<Worker> _logger;
    readonly ICitilabConnector _citilabConnector;
    readonly InquiriesJournalSettings _journalSettings;

    public Worker(
        ILogger<Worker> logger,
        ICitilabConnector citilabConnector,
        IOptions<InquiriesJournalSettings> journalSettings)
    {
        _logger = logger;
        _citilabConnector = citilabConnector;
        _journalSettings = journalSettings.Value;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);

        Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

        IInquiriesJournalFilter filter = new InquiriesJournalFilter();
        ISaver saver = new FileSaver();

        while (!stoppingToken.IsCancellationRequested)
        {
            var from = _journalSettings.StartFrom;
            var to = from + TimeSpan.FromMinutes(_journalSettings.RequestStepInMinutes);

            while(from < _journalSettings.EndTo && to <= _journalSettings.EndTo)
            {
                _logger.LogInformation($"Request from {from} to {to}");


                var payload = default(GetInquiriesJournal_response_unwrapped);
                var filteredList = default(List<Order>);

                try
                {
                    payload = await _citilabConnector.GetInquiriesJournalAsync(from, to);
                    filteredList = filter.FilterByServices(payload.O, _journalSettings.Filter);

                }
                catch (Exception e)
                {
                    _logger.LogError(e, "Exception while GetInquiriesJournalAsync calling");
                    filteredList = default(List<Order>);
                }

                try
                {
                    foreach (var order in filteredList)
                        await saver.SaveAsync($"{_journalSettings.PathToSave}{order.F.First(x => x.N == "internalId").V}.xml", order.SerializeToXml());
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "Exception while saving Orders");
                }

                from += TimeSpan.FromMinutes(_journalSettings.RequestStepInMinutes);
                to = from + TimeSpan.FromMinutes(_journalSettings.RequestStepInMinutes);

                if (to > _journalSettings.EndTo) to = _journalSettings.EndTo;
            }

            await Task.Delay(TimeSpan.FromMinutes(_journalSettings.TimeoutInMinutes), stoppingToken);
        }
    }
}
